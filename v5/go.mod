module modernc.org/cc/v5

go 1.17

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/pmezard/go-difflib v1.0.0
	modernc.org/ccorpus2 v0.0.14
	modernc.org/mathutil v1.4.1
	modernc.org/strutil v1.1.1
	modernc.org/token v1.0.0
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	modernc.org/opt v0.1.3 // indirect
)
